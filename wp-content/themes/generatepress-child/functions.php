<?php
/**
 * GeneratePress Child Theme functions and definitions
 *
 * All functions are prefixed with gpc_
 *
 * @package GenerateChild
 */

if ( ! defined( 'ABSPATH' ) ) exit;

define( 'GPC_VERSION', '1.0');

/**
 * Hide admin bar.
 */
show_admin_bar( false );

/**
 * Enqueue scripts and styles.
 */
add_action( 'wp_enqueue_scripts', 'gpc_scripts' );
function gpc_scripts() {
  wp_enqueue_style( 'gpc-base', get_stylesheet_directory_uri() . '/css/base.css', false, GPC_VERSION, 'all');
  // wp_enqueue_style( 'gpc-sidebar-header-layout', get_stylesheet_directory_uri() . '/css/sidebar-header-layout.css', false, GPC_VERSION, 'all');
  wp_enqueue_script( 'gpc-scripts', get_stylesheet_directory_uri() . '/js/scripts.js', array( 'jquery' ), GPC_VERSION, true );
}

/**
 * Enqueue matcheight from Lightweight Grid Columns if it isn't already.
 */
// add_action( 'wp_loaded', 'gpc_lgc_loaded' );
function gpc_lgc_loaded() {
	if ( function_exists( 'lgc_shortcodes_css' ) ) {
		if ( ! wp_script_is( 'lgc-matchHeight', $list = 'enqueued' ) ) {
      wp_enqueue_script( 'lgc-matchHeight', plugins_url( '/lightweight-grid-columns/js/jquery.matchHeight-min.js' ), array( 'jquery' ), GPC_VERSION, true );
    }
  }
}

/**
 * Add body classes.
 */
add_filter( 'body_class', 'gpc_body_classes' );
function gpc_body_classes( $classes ) {
  $classes[] = 'gpc';
  return $classes;
}

/**
 * Add .has-js class to html element.
 */
add_action( 'generate_before_header','gpc_add_js_class' );  
function gpc_add_js_class() { ?> 
  <script>
    jQuery('html').addClass('has-js');
  </script>
<?php }

/**
 * Responsive embedded video.
 */
add_filter( 'embed_oembed_html', 'gpc_embed_html', 10, 3 );
add_filter( 'video_embed_html', 'gpc_embed_html' ); // Jetpack
function gpc_embed_html( $html ) {
  return '<div class="video-container">' . $html . '</div>';
}

/**
 * Enable shortcodes in widgets.
 */
add_filter( 'widget_text' , 'do_shortcode' );

/**
 * Remove wrapping tags around shortcodes.
 * @link https://stackoverflow.com/questions/29862732/p-tag-formatting-in-shortcodes-content-wordpress
 */
// remove_filter( 'the_content', 'wpautop' );
// add_filter( 'the_content', 'wpautop', 99 );
// add_filter( 'the_content', 'shortcode_unautop', 100 );

/**
 * Enable field label visibility option in Gravity Forms.
 */
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

/**
 * Include other functions as needed from the `inc` folder.
 */
require get_stylesheet_directory() . '/inc/users.php';
require get_stylesheet_directory() . '/inc/generatepress.php';
require get_stylesheet_directory() . '/inc/styles.php';
require get_stylesheet_directory() . '/inc/dashboard-widgets.php';
require get_stylesheet_directory() . '/inc/sub-menu-widget.php';
require get_stylesheet_directory() . '/inc/optimizations.php';
require get_stylesheet_directory() . '/inc/cpt-output-reset.php';
require get_stylesheet_directory() . '/inc/cpt-output-custom.php';
require get_stylesheet_directory() . '/inc/image-sizes.php';
require get_stylesheet_directory() . '/inc/wp-show-posts.php';
// require get_stylesheet_directory() . '/inc/advanced-custom-fields.php';
// require get_stylesheet_directory() . '/inc/woocommerce.php';
require get_stylesheet_directory() . '/inc/shortcodes.php';
