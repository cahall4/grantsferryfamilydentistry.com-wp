<?php
/**
 * Shut off default post type and taxonomy output.
 *
 * These attempt to shut down the standard output so that we can define our own.
 * Must be included in functions.php
 *
 * @package GenerateChild
 */

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Remove default post image for single view on specified post types.
 */
add_filter( 'option_generate_blog_settings', 'gpc_remove_default_single_image' );
function gpc_remove_default_single_image() {
  $custom_post_types = array( 'our_services' );
  if ( is_singular( $custom_post_types ) ) {
    $options['single_post_image'] = false;
  }
  return $options;
}
