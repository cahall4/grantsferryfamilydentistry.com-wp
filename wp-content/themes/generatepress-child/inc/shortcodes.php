<?php
/**
 * Theme shortcodes.
 *
 * Create custom shortcodes to use in theme.
 * Must be included in functions.php
 *
 * @package GenerateChild
 * @link https://codex.wordpress.org/Shortcode_API
 */

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Appointment Request Form.
 * [site_appointment_request_form]
 */
function site_appointment_request_form_func() {
  ob_start(); ?>
    <script type="text/javascript">
      var RW_Config = {
        id: '0a144099-42c1-41ce-83ba-5fafa5874563',
        healthgrades: 'false',
        host: 'https://s1.revenuewell.com',
        ra_header_color: '#686868',
        ra_header_off: 0,
        ra_bold_text_color: '#5c5c5c',
        ra_text_color: '#5c5c5c',
        ra_font_family: 'Arial',
        ra_bg: 'transparent'
      };
    </script>
    <script src="https://s1.revenuewell.com/scripts/js-api.js" type="text/javascript"></script>
    <!--this DIV inserts the appointment request form -->
    <div id="RwRequestAppointment"></div>
  <?php
  return ob_get_clean();
}
add_shortcode( 'site_appointment_request_form', 'site_appointment_request_form_func' );

/**
 * Appointment Request Form.
 * [site_show_reviews]
 */
function site_show_reviews_func() {
  ob_start(); ?>
    <script type="text/javascript">
      var RW_Config = {
        id: '0a144099-42c1-41ce-83ba-5fafa5874563',
        host: 'https://s1.revenuewell.com',
        rv_header_color: '#000',
        rv_text_color: '#4c4c4c',
        rv_testimonials_color: '#910100',
        rv_page_size: 5,
        rv_iframe_hight: 1000
      };
    </script>
    <script src="https://s1.revenuewell.com/scripts/js-api.js" type="text/javascript"></script>
    <!--this DIV inserts the reviews widget -->
    <div id="RwReview"></div>
  <?php
  return ob_get_clean();
}
add_shortcode( 'site_show_reviews', 'site_show_reviews_func' );

/**
 * Output services menu list.
 * [site_services_menu]
 */
function site_output_services_menu_func() {
  ob_start();
  $args = array (
    'post_type' => 'our_services',
    'order' => 'ASC',
    'orderby' => 'menu_order',
    'posts_per_page' => -1
  );
  $query = new WP_Query( $args );
  global $post;
  $current_id = $post->ID;
  if ( $query->have_posts() ) :
    echo '<ul class="our-services-list">';
    while ( $query->have_posts() ) : $query->the_post();
      if ( $current_id == get_the_id() ) {
        $class = 'current-item';
      } else {
        $class = '';
      }
      echo '<li class="' . $class . '"><a href="' . get_the_permalink() . '">' . get_the_title() . '</a></li>';
    endwhile;
    echo '</ul>';
    wp_reset_postdata();
    $output = ob_get_clean();
    return $output;
  endif;
}
add_shortcode( 'site_services_menu', 'site_output_services_menu_func' );

/**
 * Output services.
 * [site_services_list category='category-slug' tag='tag-slug']
 */
function site_services_list_func( $atts ) {
  $a = shortcode_atts( array(
    'category' => '',
    'tag' => '',
  ), $atts );
  $cpt_name = 'our_services';
  $cat_name = 'service_categories';
  $cat_slug = $a[ 'category' ];
  $tag_name = 'service_tags';
  $tag_slug = $a[ 'tag' ];
  $taxonomy_array[] = '';
  $no_results_string = 'There are no results.';
  if ( $cat_slug ) {
    $taxonomy_array[] = array (
      'taxonomy' => $cat_name,
      'field' => 'slug', // can be 'term_id', 'name', 'slug' or 'term_taxonomy_id'
      'terms' => $cat_slug
    );
  }
  if ( $tag_slug ) {
    $taxonomy_array[] = array (
      'taxonomy' => $tag_name,
      'field' => 'slug', // can be 'term_id', 'name', 'slug' or 'term_taxonomy_id'
      'terms' => $tag_slug
    );
  }
  if ( $taxonomy_array ) {
    $args = array (
      'post_type' => $cpt_name,
      'order' => 'ASC',
      'orderby' => 'menu_order',
      'posts_per_page' => -1,
      'tax_query' => array( $taxonomy_array )
    );  
  } else {
    $args = array (
      'post_type' => $cpt_name,
      'order' => 'ASC',
      'orderby' => 'menu_order',
      'posts_per_page' => -1
    );
  }
  $query = new WP_Query( $args );
  if ( $query->have_posts() ) {    
    ob_start();
    echo '<div class="grid-container grid-parent flex flex-wrap justify-center">';
    while ( $query->have_posts() ) : $query->the_post();
      include get_stylesheet_directory() . '/partials/content-services-archive.php';
    endwhile;
    echo '</div>';
    wp_reset_postdata();
    $output = ob_get_clean();
  } else {
    $output = $no_results_string;
  }  
  return $output;
}
add_shortcode( 'site_services_list', 'site_services_list_func' );

/**
 * Output team.
 * [site_team_list]
 */
function site_team_list_func() {
  $cpt_name = 'team';
  $no_results_string = 'There are no results.';
  $count = 1;
  $args = array (
    'post_type' => $cpt_name,
    'order' => 'ASC',
    'orderby' => 'menu_order',
    'posts_per_page' => -1
  );
  $query = new WP_Query( $args );
  if ( $query->have_posts() ) {    
    ob_start();
    echo '<div class="team-posts grid-parent grid-reset flex flex-wrap">';
    while ( $query->have_posts() ) : $query->the_post();
      include get_stylesheet_directory() . '/partials/content-team-list.php';
      $count++;
    endwhile;
    echo '</div>';
    wp_reset_postdata();
    $output = ob_get_clean();
  } else {
    $output = $no_results_string;
  }  
  return $output;
}
add_shortcode( 'site_team_list', 'site_team_list_func' );