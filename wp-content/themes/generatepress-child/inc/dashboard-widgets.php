<?php
/**
 * Add widgets to the dashboard.
 *
 * Must be included in functions.php
 *
 * THE FOLLOWING ARE ALL ONLY EXAMPLES. DUPLICATE AND MODIFY AS NEEDED.
 *
 * @package GenerateChild
 */
 
/**
 * Display video tutorials link.
 */
function gpc_tutorials_dashboard_widget() {
  wp_add_dashboard_widget(
    'gpc_tutorials_dashboard_widget', // Widget slug.
    'Video Tutorials', // Title.
    'gpc_tutorials_dashboard_widget_function' // Display function.
  );	
}
add_action( 'wp_dashboard_setup', 'gpc_tutorials_dashboard_widget' );
function gpc_tutorials_dashboard_widget_function() {
  echo '<p>You may access video tutorials for this website here:</p>
  <p><a href="https://vimeo.com/album/4889024" target="_blank">https://vimeo.com/album/4889024</a><br>
  Password: greentooth</p>';
}

/**
 * Display custom shortcodes.
 */
function gpc_shortcodes_dashboard_widget() {
  wp_add_dashboard_widget(
    'gpc_shortcodes_dashboard_widget', // Widget slug.
    'Shortcodes', // Title.
    'gpc_shortcodes_dashboard_widget_function' // Display function.
  );	
}
add_action( 'wp_dashboard_setup', 'gpc_shortcodes_dashboard_widget' );
function gpc_shortcodes_dashboard_widget_function() {
  echo '<p>Any custom shortcodes for use on your site will appear here.</p>';
  echo '<dl>';
  echo '<dt>[site_services_list category="category-slug" tag="tag-slug"]</dt>';
  echo '<dd>Output a list of services with images. Filter by category or tag.</dd>';
  echo '<dt>[site_services_menu]</dt>';
  echo '<dd>Output a menu list of services with links.</dd>';
  echo '<dt>[site_team_list]</dt>';
  echo '<dd>Output a menu list of team members.</dd>';
  echo '<dt>[site_appointment_request_form]</dt>';
  echo '<dd>Output an appointment request form.</dd>';
  echo '<dt>[site_show_reviews]</dt>';
  echo '<dd>Output patient reviews.</dd>';
  echo '</dl>';
}