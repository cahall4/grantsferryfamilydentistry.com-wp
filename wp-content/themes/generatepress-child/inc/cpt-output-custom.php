<?php
/**
 * Custom post type output.
 *
 * These filters and actions tell WordPress to do things
 * like use partials, use custom sort orders, or even add 
 * custom menu classes for specified post types and taxonomies.
 * Must be included in functions.php
 *
 * @package GenerateChild
 * @see /inc/cpt-output-reset.php
 */

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Re-insert feature image on our_services post type.
 */
add_action( 'the_content', 'gpc_services_feature_image' );
function gpc_services_feature_image( $content ) {
  if ( is_singular( 'our_services' ) ) {
    if ( has_post_thumbnail() ) :
      ob_start(); ?>
      <div class="service-image mobile-center tablet-right desktop-right tablet-ml2 desktop-ml3">
        <span class="service-image-inner circle mobile-mx-auto" style="background-image: url('<?php the_post_thumbnail_url( 'medium' ); ?>');">
          &nbsp;
        </a>
      </div>
      <?php
      $html_img = ob_get_contents();
      ob_end_clean();
    endif;
    return $html_img . $content;
  } else {
    return $content;
  }
}
