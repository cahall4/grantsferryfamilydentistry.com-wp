<?php
/**
 * Partial for outputting services.
 *
 * @package GenerateChild
 * @see /inc/cpt-output-custom.php
 */

if ( ! defined( 'ABSPATH' ) ) exit; ?>

<article class="grid-33 tablet-grid-50 mobile-grid-100 center service-item flex" data-mh="services-group">
  <div class="service-item-inner service-item-inner-corners">
    <?php if ( has_post_thumbnail() ) : ?>
      <div class="mb2 service-image center">
        <a href="<?php the_permalink(); ?>" class="service-image-inner circle mx-auto" style="background-image: url('<?php the_post_thumbnail_url( 'medium' ); ?>');">
          &nbsp;
        </a>
      </div>
    <?php endif; ?>
    <h3 class="service-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
    <div class="service-description"><?php the_excerpt(); ?></div>
  </div>
</article>
