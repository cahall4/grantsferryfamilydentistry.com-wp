<?php
/**
 * Partial for outputting team.
 *
 * @package GenerateChild
 * @see /inc/cpt-output-custom.php
 */

if ( ! defined( 'ABSPATH' ) ) exit; ?>

<?php
  if ( isset( $count ) && $count == 1 ) {
    $grid_classes = 'grid-100 first';
  } else {
    $grid_classes = 'grid-50 tablet-grid-50 mobile-grid-100';
  }
?>

<div class="<?php echo $grid_classes; ?> mb3">
  <?php if ( has_post_thumbnail() ) : ?>
    <?php the_post_thumbnail( 'large', array( 'class' => 'team-photo desktop-left' ) ); ?>
  <?php endif; ?>
  <h3><?php the_title(); ?></h3>
  <?php the_content(); ?>
</div>
